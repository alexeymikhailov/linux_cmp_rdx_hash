obj-m += cmp_rdx_hash.o

all:
	make -C /lib/modules/$(shell uname -r)/build/ M=$(PWD) modules

mbench:
	$(MAKE) -C /lib/modules/$(shell uname -r)/build/ M=$(PWD) EXTRA_CFLAGS="-DMICROBENCH" modules

clean:
	make -C /lib/modules/$(shell uname -r)/build/ M=$(PWD) clean
