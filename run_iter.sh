#!/bin/sh

echo "Running"

insmod cmp_rdx_hash.ko
rmmod cmp_rdx_hash

echo "Grabbing statistics"

grep MRADIX /var/log/kern.log|tail -100| awk '{print $(NF-1), $NF}' > radix_perf.dat
grep MHASH /var/log/kern.log|tail -100|awk '{print $(NF-1), $NF}' > hash_perf.dat

echo "Plotting"

gnuplot plot_cmp.gnuplot

echo "Saving"
ts=$(date +%Y%m%d%H%M%S)
mv radix_perf.dat stats/radix_perf.$ts.dat
mv hash_perf.dat stats/hash_perf.$ts.dat
mv tmp_plot.png stats/plot.$ts.png

echo "Done"
