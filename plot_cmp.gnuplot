set autoscale
unset log
unset label
set xtic auto
set ytic auto
set xlabel "Size"
set ylabel "Time"
set term png
set output "tmp_plot.png"

plot    "radix_perf.dat" using 1:2 title 'radix' with linespoints, \
	"hash_perf.dat" using 1:2 title 'hash' with linespoints


