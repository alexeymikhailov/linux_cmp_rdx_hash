/*
 * cmp_rdx_hash: simple kernel module that measures radix and hashtables
 *               performance
 *
 * Copyright (C) 2018 Alexey Mikhailov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/random.h>
#include <linux/slab.h>
#include <linux/radix-tree.h>
#include <linux/hashtable.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alexey Mikhailov <alexey.mikhailov@gmail.com>");
MODULE_DESCRIPTION("Measure performance (insertion): hash table and radix");
MODULE_VERSION("0.1");

#ifdef MICROBENCH
static unsigned long step = 1000;
#endif

static unsigned long nelems = 100 * 1000;
static unsigned long *rnd_buf = NULL;

#define HT_ULONG_BITS 10

struct ulong_h {
        struct hlist_node hash;

        unsigned long key;
};

static DEFINE_HASHTABLE(ulong_hash, HT_ULONG_BITS);

static int measure_hash_insert (void)
{
        unsigned long i;
        int bkt;
        struct timeval tvs, tve;
        unsigned long *bp = rnd_buf;
        struct ulong_h *ulh;
        struct hlist_node *tmp;

#ifdef MICROBENCH
        struct timeval *tvi;

        tvi = kmalloc(sizeof(struct timeval) * (nelems/step), GFP_KERNEL);

        if (tvi == NULL)  {
                printk(KERN_ERR "measure_hash_insert: kmalloc() failed");
                return -ENOMEM;
        }

        printk(KERN_DEBUG "measure_hash_insert: microbench is on, step = %lu\n",
                step);
#endif

        printk(KERN_DEBUG "measure_hash_insert: populating\n");
        do_gettimeofday(&tvs);

        for (i = 0; i < nelems; i++) {
                struct ulong_h *ulh = kmalloc(sizeof(struct ulong_h), GFP_KERNEL);
                INIT_HLIST_NODE(&ulh->hash);

#ifdef MICROBENCH
                if ((i+1) % step == 0) {
                        do_gettimeofday(&tvi[(i+1)/step - 1]);

                }
#endif
                ulh->key = *bp;
                hash_add(ulong_hash, &ulh->hash ,*bp);
                bp++;
        }


        do_gettimeofday(&tve);
        printk(KERN_INFO "measure_hash_insert: insertion took %ld milliseconds\n",
               1000 * (tve.tv_sec - tvs.tv_sec) +
               (tve.tv_usec - tvs.tv_usec) / 1000);

#ifdef MICROBENCH
        unsigned long val = step;
        for (i = 0; i < (nelems/step); i++) {
                printk(KERN_DEBUG "MHASH %lu %lu\n",
                       val,
                       1000000 * (tvi[i].tv_sec - tvs.tv_sec) +
                       (tvi[i].tv_usec - tvs.tv_usec));
                val += step;

        }
        kfree(tvi);
#endif

        printk(KERN_DEBUG "measure_hash_insert: clearing up\n");
        i = 0;
        hash_for_each_safe(ulong_hash, bkt, tmp, ulh, hash) {
                hash_del(&(ulh->hash));
                kfree(ulh);
                i++;
        }
        printk(KERN_DEBUG "measure_hash_insert: done (%ld entries)\n", i);

        return 0;
}

static int measure_radix_insert (void)
{
        unsigned long i;
        struct timeval tvs, tve;
        unsigned long *bp = rnd_buf;
        struct radix_tree_root rtree;

#ifdef MICROBENCH
        struct timeval *tvi;

        tvi = kmalloc(sizeof(struct timeval) * (nelems/step), GFP_KERNEL);

        if (tvi == NULL)  {
                printk(KERN_ERR "measure_radix_insert: kmalloc() failed");
                return -ENOMEM;
        }

        printk(KERN_DEBUG "measure_radix_insert: microbench is on, step = %lu\n",
                step);
#endif

        INIT_RADIX_TREE(&rtree, GFP_KERNEL);

        printk(KERN_DEBUG "measure_radix_insert: populating\n");
        do_gettimeofday(&tvs);

        for (i = 0; i < nelems; i++) {
#ifdef MICROBENCH
                if ((i+1) % step == 0) {
                        do_gettimeofday(&tvi[(i+1)/step - 1]);

                }
#endif
                radix_tree_insert(&rtree, *bp, (void *)bp);
                bp++;
        }


        do_gettimeofday(&tve);
        printk(KERN_INFO "measure_radix_insert: insertion took %ld milliseconds\n",
               1000 * (tve.tv_sec - tvs.tv_sec) +
               (tve.tv_usec - tvs.tv_usec) / 1000);

#ifdef MICROBENCH
        unsigned long val = step;
        for (i = 0; i < (nelems/step); i++) {
                printk(KERN_DEBUG "MRADIX %lu %lu\n",
                       val,
                       1000000 * (tvi[i].tv_sec - tvs.tv_sec) +
                       (tvi[i].tv_usec - tvs.tv_usec));
                val += step;

        }
        kfree(tvi);
#endif

        printk(KERN_DEBUG "measure_radix_insert: freeing tree\n");
        bp = rnd_buf;
        for (i = 0; i < nelems; i++) {
                radix_tree_delete(&rtree, *bp);
                bp++;
        }
        printk(KERN_DEBUG "measure_radix_insert: done\n");

        return 0;
}

static int fill_rnd_buf (void)
{
        size_t size = sizeof(unsigned long) * nelems;
        unsigned long *p;
        int i;

        printk(KERN_DEBUG "fill_rnd_buf: allocating %lu (%ld x %lu) bytes\n",
               size, sizeof(unsigned long), nelems);
        rnd_buf = kmalloc(size, GFP_KERNEL);

        if (rnd_buf == NULL) {
                printk(KERN_ERR "fill_rnd_buf: kmalloc() failed\n");
                return -ENOMEM;
        }

        p = rnd_buf;

        printk(KERN_DEBUG "fill_rnd_buf: generating input data\n");

        for (i = 0; i < nelems; i++) {
                get_random_bytes(p, sizeof(unsigned long));
                p++;
        }

        printk(KERN_DEBUG "fill_rnd_buf: done\n");

        return 0;
}

#if 0
/* TODO: configurable load-time params? */
static void check_mod_params (void)
{

}
#endif

static int __init cmp_rdx_hash_init (void)
{
        int err;

        printk(KERN_INFO "cmp_rdx_hash: init\n");

        err = fill_rnd_buf();
        if (err)
                return err;

        err = measure_radix_insert();
        if (err)
                goto free_rnd_buf;

        err = measure_hash_insert();
        if (err)
                goto free_rnd_buf;

        return 0;

free_rnd_buf:
        kfree(rnd_buf);
        return -1;
}

static void __exit cmp_rdx_hash_exit (void)
{
        if (rnd_buf)
                kfree(rnd_buf);

        printk(KERN_INFO "cmp_rdx_hash: exit\n");
}

module_init(cmp_rdx_hash_init);
module_exit(cmp_rdx_hash_exit);
